import { ReactElement } from 'react';
import logo from './logo.svg';
import './App.css';
import { useHeavyContent } from './hooks/useHeavyContent';
import { generateHeavyContent } from './utils/generateHeavyContent';
import { HeavyContentList } from './components/HeavyContentList'


function App(): ReactElement {
    // d - List of items that can be updated with new additional items when the button is pressed
    var [heavyList, addNewHeavyContentItem] = useHeavyContent(generateHeavyContent)

    return (
        <div className="App">
            <div className="App-header">
                <img src={logo} className="App-logo" alt="logo"/>
            </div>
            <div>
                <button className={"App-button"} onClick={addNewHeavyContentItem}>
                    Add More
                </button>
            </div>
            <div>
                <HeavyContentList>
                    {heavyList}
                </HeavyContentList>
            </div>
        </div>
    );
}

export default App;
