export function getRandomInteger(power: number): number {
    return Math.floor(Math.random() * Math.pow(10, power))
}
