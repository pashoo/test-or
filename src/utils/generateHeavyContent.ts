import { IHeavyContentItem } from '../types/heavyContent';
import { getRandomInteger } from './getRandomInteger';
import { getStringChecksum } from './getStringChecksum';

export function generateHeavyContent(): IHeavyContentItem {
    const heavyStringLength: number = getRandomInteger(3)
    let heavyString: string = ''
    let i = 0
    while(i < heavyStringLength) {
        heavyString = heavyString + String.fromCharCode(getRandomInteger(1))
        i+=1
    }
    const heavyStringChecksum: number = getStringChecksum(heavyString)
    return {
        id: heavyStringChecksum,
        content: encodeURIComponent(heavyString),
    }
}
