// https://stackoverflow.com/a/33647870
export function getStringChecksum(str: string): number {
    var hash = 0, i = 0, len = str.length;
    while ( i < len ) {
        hash  = ((hash << 5) - hash + str.charCodeAt(i++)) << 0;
    }
    return hash;
}
