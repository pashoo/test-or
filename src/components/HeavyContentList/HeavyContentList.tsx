import { FC, memo, ReactElement } from 'react';
import { IHeavyContentItem, THeavyContent } from '../../types/heavyContent';
import { HeavyContentItem } from '../HeavyContentItem/HeavyContentItem';

interface IHeavyContentListProps {
    children: THeavyContent
}

export const HeavyContentList: FC<IHeavyContentListProps> = memo(({
    children
}): ReactElement => {
    const renderHeavyItem = (item: IHeavyContentItem, idx: number): ReactElement => (
        <div key={item.id}>
            ({ idx }). <HeavyContentItem item={item} />
        </div>
    )
    return (
        <div>
            {children.map(renderHeavyItem)}
        </div>
    )
})
