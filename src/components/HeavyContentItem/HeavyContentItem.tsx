import { FC, memo, ReactElement, ReactNode, useCallback, useMemo } from 'react';
import { useWhetherNodeVisible } from '../../hooks/useWhetherNodeVisible';
import { IHeavyContentItem } from '../../types/heavyContent';

interface IHeavyContentItemProps {
    item: IHeavyContentItem
}

export const HeavyContentItem: FC<IHeavyContentItemProps> = memo(({
    item,
}): ReactElement => {
    const { content } = item;

    const [isVisible, setElementReference] = useWhetherNodeVisible()

    const onReferenceChanged = useCallback((ref: HTMLSpanElement | undefined | null): void => {
        setElementReference(ref ?? null)
    }, [setElementReference])
    const contentToRender = useMemo((): ReactNode => isVisible && content, [content, isVisible])
    // TODO - issue with ref type https://github.com/DefinitelyTyped/DefinitelyTyped/issues/35572
    return <span ref={onReferenceChanged as any}>{ contentToRender }</span>
})
