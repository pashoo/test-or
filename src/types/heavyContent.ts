export interface IHeavyContentItem {
    content: string,
    id: number,
}
export type THeavyContent = IHeavyContentItem[]

export interface IAddNewHeavyContentItem {
    (): IHeavyContentItem;
}
