import { useEffect, useState } from 'react'

export function useWhetherNodeVisible(): [boolean, (domNodeOrNull: Element | null) => void] {
    const [domNodeReferenceOrNull, setDomNodeReference] = useState<Element | null>(null)
    const [isIntersecting, setIntersecting] = useState(false)
    const [intersectionObserverInstanceOrNull, setIntersectionObserverInstance] = useState<IntersectionObserver | null>(null)
  
    useEffect(() => {
        const newIntersectionObserverInstance = new IntersectionObserver(
            ([entry]) => {
                setIntersecting(entry.isIntersecting)
            }
          )
        setIntersectionObserverInstance(newIntersectionObserverInstance)
        return () => {
            newIntersectionObserverInstance.disconnect()

        }
    }, [])
    useEffect(() => {
        if (intersectionObserverInstanceOrNull && domNodeReferenceOrNull) {
            intersectionObserverInstanceOrNull.observe(domNodeReferenceOrNull)
            return () => { 
                intersectionObserverInstanceOrNull.unobserve(domNodeReferenceOrNull)
                setIntersecting(false)
            }
        }
    }, [domNodeReferenceOrNull, intersectionObserverInstanceOrNull])
  
    return [isIntersecting, setDomNodeReference]
  }