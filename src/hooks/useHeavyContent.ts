import { useCallback, useState } from 'react'
import { IAddNewHeavyContentItem, THeavyContent } from '../types/heavyContent';

export function useHeavyContent(
    generateNewItem: IAddNewHeavyContentItem
): [THeavyContent, () => void] {
    const [content, setContent] = useState<THeavyContent>([])
    const addNewItemIntoHeavyContent = useCallback((): void => {
        setContent([
            ...content,
            generateNewItem(),
        ])
    }, [content, generateNewItem]);

    return [content, addNewItemIntoHeavyContent];
}
